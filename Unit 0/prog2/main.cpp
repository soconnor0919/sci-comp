// My Second Program in C++!
#include <iostream>
using namespace std;

int main() {
    cout << "Hello World! ";        // Prints "Hello World!" using std::cout
    cout << "I'm a C++ program!";   // Prints "I'm a C++ program! using cout
}
