// Fizz Buzz Project
// @author Sean O'Connor

#include <iostream>

using namespace std;

int main() {
	for (int i = 1; i <= 100; i++) { // Runs 100 times (starts i at 1, interates value when done)
		if ((i % 15) == 0) { // Checks if $current_number is evenly divisible by 15 using modulo
			cout << "FizzBuzz" << endl; // If so, print "FizzBuzz"
		} else if ((i % 5) == 0) { // Checks if $current_number is evenly divisible by 5 using modulo
			cout << "Buzz" << endl; // If so, print "Buzz"
		} else if ((i % 3) == 0) { // Checks if $current_number is evenly divisible by 3 using modulo
			cout << "Fizz" << endl; // If so, print "Fizz"
		} else { // If the number was not evenly divisible by 3, 5, or 15:
			cout << i << endl;  // Print $current_number
		}
	}
}
