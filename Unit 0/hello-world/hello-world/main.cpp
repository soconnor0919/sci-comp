/* hello-world.cpp
Now including Block Comments! */

#include <iostream>

using namespace std;

double x = 2.432; // a double has decimal places
bool y = false;   // booleans have 2 options, true and false
int z = 2;        // an integer is a single digit
string w = "This is a string";

int main()
{
    cout << "Double 'x' is equal to " << x << "." << endl;
    cout << "Integer 'z' is equal to " << z << "." << endl;
    cout << "Boolean 'y' is set to " << y << "." << endl;
    cout << "String 'w' is set to " << w << "." << endl;
    cout << "Hello World!" << endl;      /* Prints "Hello World!" using std::cout */
    cout << "I'm a C++ program" << endl; /* Prints more using the same method */
}
