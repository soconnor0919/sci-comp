// Create an adventure project
// @author Sean O'Connor

#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

// printf() is used here a lot, it is just like cout but with more customization, and mainly I just prefer it.

// Function Declarations
void choicePrompt(const char *x, int num);
void endingHeader(const char *x);
void gameOver();
int getAnswer(int maxChoices);
void intro();
void pause();
void printDash(int i);
void questionPrompt();
int main();

// Takes in two choice strings, and prints them in the correct format
void choicePrompt2(const char *x, const char *y){
    printf("1. %s\n", x);   // Inside the printf() statement, there are two main components. What is actually printed, and the variables. Inside the quotes it prints the number one, a period, a space, then holds a space for a string (represented by %s) and finally prints a newline.
    printf("2. %s\n\n", y);
}

// Takes in three choice strings, and prints them in the correct format
void choicePrompt3(const char *x, const char *y, const char *z){
    printf("1. %s\n", x);
    printf("2. %s\n", y);
    printf("3. %s\n\n", z);
}

// Takes in four choice strings, and prints them in the correct format
void choicePrompt4(const char *x, const char *y, const char *z, const char *a){
    printf("1. %s\n", x);
    printf("2. %s\n", y);
    printf("3. %s\n", z);
    printf("4. %s\n\n", a);
}

// Prints a custom header for an ending
void endingHeader(const char *x){   // Takes in ending title
    int length = strlen(x);    // Checks the length of the string
    printDash(length);  // Prints - once for each character of the string above and below the string (dynamic length)
    printf("%s\n", x);
    printDash(length);
    
}

// Asks the user if they want to play again.
void gameOver(){
input:
    printf("\n\nGame Over! Play Again? (y/n):");
    char input[64];
    fgets(input, 64, stdin);    // Just like cin, but with a byte restriction
    if(strcmp(input,"y\n") == 0 || strcmp(input,"Y\n") == 0){   // Compares input to strings 
        main();
    } else if(strcmp(input,"n\n") == 0 || strcmp(input,"N\n") == 0){
        exit(0);
    } else {
        printf("\nInvalid Input. Try again:\n");
        goto input;
    }
}

// Returns answer from user
int getAnswer(int maxChoices){  // Takes in the maximum possible amount of choices
    int ans;
    char input[64];
input:
    printf("Choose 1 - %d: ", maxChoices);  // Prints "Input 1 - (whatever the maximum number is)"
    fgets(input, 64, stdin);    // Just like cin, but with a byte restriction
    ans = atoi(input);  // Type conversion, converts the inputted string into an integer
    if(ans >= 1 && ans <= maxChoices){  // Checks if the integer fits the allowed range of inputs
        return ans;     // If so, return the answer
    } else {
        printf("\nInvalid Input, try again:\n");    // If not, try again.
        goto input;
    }
}

// Prints an intro screen
void intro(){
    system("clear");    // Clears the screen
    endingHeader("Create an Adventure Project");    // Although this is not an ending, it uses the same formatting, so the same method is used.
    printf("By Sean O'Connor.\n\nIf you would like to read the script, check classroom for a link \nto a document where I initially wrote the dialogue/story. Have fun!");  // Prints an intro message.
    pause();    // Waits for return/enter key.
}

// Waits for the user to press return.
void pause(){
    char ch;
    printf("\n\nPress ENTER key to Continue.\n");  
    scanf("%c",&ch);    // Scans for the enter key.
}

// Prints a specified number of dashes
void printDash(int i){
    for(i; i>0; i--){
        printf("-");
    }
    printf("\n");
} 

// Takes in a question string and prints it in the correct format
void questionPrompt(const char *x){
    printf("\n\n%s\n",x);
}

int main() {
    int uuid = 5208;    // UUIDs (Universal Unique IDentifiers) are used to identify each question
    intro();    // Runs the intro sequence
    while(uuid!=1){     // Checks if the UUID is equal to 1. If it is 1, the switch case stops looping.
        system("clear");    // Clears the screen
        switch(uuid){   // Switches on the UUID
            // As all cases are basically the same, I will explain one basic question, one special question and one ending.    
            case 5208: // Case (UUID)
                printf("You wake up in a room with few furnishings: a bed, a tall clear \ncylindrical object of unknown origin, a door, and a window that \nappears to be facing the dark night."); // Prints the prompt, "\n" represents a newline.
                questionPrompt("What do you do first?");   // Asks the question
                choicePrompt3("Open the window.", "Inspect the tall cylinder.", "Go through the door.");    // Lists answers
                switch(getAnswer(3)){   // Takes in answer variable and changes the UUID accordingly.
                    case 1:
                        uuid = 8039;
                        break;
                    case 2:
                        uuid = 2464;
                        break;
                    case 3:
                        uuid = 8891;
                        break;
                }
                break;  // Ends the switch case.
            case 8039:
                printf("You open the window, and find a black cloth blocking the \noutside world.");
                questionPrompt("Do you:");
                choicePrompt3("Rip off the cloth, and climb through the window.", "Close the window, and go through the door.", "Close the window, and go to the tall cylinder.");
                switch(getAnswer(3)){
                    case 1:
                        uuid = 5589;
                        break;
                    case 2:
                        uuid = 8891;
                        break;
                    case 3:
                        uuid = 2464;
                        break;
                }
                break;
            case 5589:
                printf("You go through the window, and find yourself on an outside \nbalcony off of a tall building in a city.");
                questionPrompt("Do you:");
                choicePrompt4("Climb to the ground.", "Climb to the roof.", "Go back in, and open the door.", "Go back in, and inspect the unknown object.");
                switch(getAnswer(4)){
                    case 1:
                        uuid = 1986;
                        break;
                    case 2:
                        uuid = 1914;
                        break;
                    case 3:
                        uuid = 8891;
                        break;
                    case 4:
                        uuid = 2464;
                        break;
                }
                break;
            case 1986:
                printf("You set foot on the ground, and find yourself in a large city.");
                questionPrompt("Do you:");
                choicePrompt3("Reenter the building on the ground floor.", "Climb back to the roof.", "Roam the city and go home.");
                switch(getAnswer(3)){
                    case 1:
                        uuid = 4715;
                        break;
                    case 2:
                        uuid = 1914;
                        break;
                    case 3:
                        uuid = 4434;
                        break;
                }
                break;
            case 4715:
                printf("You opened the front door, and briefly spotted a vault door and \nsome stairs before you got knocked out by a guard. You wake up in what \nyou think is an interrogation room, with a metal table and two cups of \nwhat appears to be the same liquid.");
                questionPrompt("Do you:");
                choicePrompt2("Drink the first cup.", "Drink the second cup.");
                switch(getAnswer(2)){
                    case 1:
                        uuid = 3216;
                        break;
                    case 2:
                        uuid = 5208;
                        break;
                }
                break;
            case 3216:
                printf("You go through Terrigenesis: Your body is encased in rock, and \nyou emerge with an unknown superpower.");
                questionPrompt("What is your power?");
                choicePrompt2("Teleportation, but to an unknown place.", "Telekinesis, but uncontrollable and randomly timed.");
                switch(getAnswer(2)){
                    case 1:
                        uuid = 5387;
                        break;
                    case 2:
                        uuid = 8454;
                        break;
                }
                break;
            case 8454:
                printf("You can now move things with your mind.");
                questionPrompt("What happens next?");
                choicePrompt2("You open the door and go through.", "You press a button that appears on the table.");
                switch(getAnswer(2)){
                    case 1:
                        uuid = 8891;
                        break;
                    case 2:
                        uuid = 5387;
                        break;
                }
                break;
            case 1914:
                printf("You climb to the roof, and see a helicopter with a pilot waiting \nfor someone. He says to you 'I will take you wherever you ask, but my /nboss would like to meet you.'");
                questionPrompt("Do you:");
                choicePrompt2("Ask him to take you home.", "Have him take you to his boss.");
                switch(getAnswer(2)){
                    case 1:
                        uuid = 4434;
                        break;
                    case 2:
                        uuid = 1010;
                        break;
                }
                break;
            case 1010:
                printf("He takes you to a building with a front and a side door. The \npilot tells you to choose one.");
                questionPrompt("Which do you go through?");
                choicePrompt2("The front door.", "The side door.");
                switch(getAnswer(2)){
                    case 1:
                        uuid = 4715;
                        break;
                    case 2:
                        uuid = 5747;
                        break;
                }
                break;
            case 5747:
                printf("You open the door, and enter. A mysterious man appears (who reminds you of the G-Man from Half Life), and he asks you if you would like the truth or to become powerful.");
                questionPrompt("What do you choose?");
                choicePrompt2("Truth.", "Power.");
                switch(getAnswer(2)){
                    case 1:
                        uuid = 3148;
                        break;
                    case 2:
                        uuid = 3216;
                        break;
                }
                break;
            case 2464:
                printf("You inspect the tall cylinder, and see that it is a teleporter.");
                questionPrompt("What do you do?");
                choicePrompt3("Go inside and press the blinking button.", "Go back to the window.", "Go through the door.");
                switch(getAnswer(3)){
                    case 1:
                        uuid = 5387;
                        break;
                    case 2:
                        uuid = 8039;
                        break;
                    case 3:
                        uuid = 8891;
                        break;
                }
                break;
            case 5387:
                printf("You appear in a desert, with a camel by a small puddle. (He’s \nwearing a funny hat, but that’s irrelevant.) You see a bunch of food \non a table.");
                questionPrompt("Do you:");
                choicePrompt3("Explore the desert.", "Eat some food.", "Steal his hat.");
                switch(getAnswer(3)){
                    case 1:
                        uuid = 9190;
                        break;
                    case 2:
                        uuid = 5917;
                        break;
                    case 3:
                        uuid = 5758;
                        break;
                }
                break;
            case 9190:
                printf("As you explore the desert, you come across a monolith. It is in \na glass box, reinforced with a foreign forcefield. It looks extremely \nout of place. There is a door on the box.");
                questionPrompt("Do you:");
                choicePrompt2("Open the door and enter the box.", "Go back to meet the camel and eat his food.");
                switch(getAnswer(2)){
                    case 1:
                        uuid = 8391;
                        break;
                    case 2:
                        uuid = 5917;
                        break;
                }
                break;
            case 5917:
                printf("You go over to the table in the middle of the desert with \ndelicious-looking food. Among others, there is a plate of cookies, and \na cupcake with glowing icing. The icing seems to be making a pulsing \nsound.");
                questionPrompt("Do you:");
                choicePrompt2("Eat a cookie.", "Eat the cupcake.");
                switch(getAnswer(2)){
                    case 1:
                        uuid = 6012;
                        break;
                    case 2:
                        uuid = 6098;
                        break;
                }
                break;
            case 6012:
                printf("You eat the cookie, and you feel yourself teleport again. You \nfeel like you are about to pass out, but before you can see what \nhappened, you fall on the floor.");  // Special prompt that has no choices, instead it runs an ending directly after it prints.
                uuid = 5208;    // Sets next UUID.
                pause();    // Waits for the enter key.
                break;
            case 8891:
                printf("You went through the door, and found yourself in a dark room. \nThe wall has an opening towards a hallway.");
                questionPrompt("Do you:");
                choicePrompt2("Go back through the door.", "Go through the hallway");
                switch(getAnswer(2)){
                    case 1:
                        uuid = 4025;
                        break;
                    case 2:
                        uuid = 9754;
                        break;
                }
                break;
            case 4025:
                printf("It’s too late now. ");
                uuid = 9754;
                break;
            case 9754:
                printf("You go through the hallway, and find yourself in a maze. \nAfter an hour or two, you find the exit. There are now two ways out: A plain \nblack door labeled exit, and A bright flashing door that has a big \nsign labeled EXIT with speakers playing loud music.");
                questionPrompt("Which one do you go through?");
                choicePrompt2("The flashy one.","The dull one.");
                switch(getAnswer(2)){
                    case 1:
                        uuid = 3972;
                        break;
                    case 2:
                        uuid = 9109;
                        break;
                }
                break;
            case 4434:
                endingHeader("THE BORING ENDING:"); // Prints ending title
                printf("You’ve escaped the trial. It was quick and \nuneventful, and you will remember it as nothing but a weird dream.");  // Prints ending prompt.
                uuid = 1;   // Sets UUID to 1, which means that the ending was reached.
                break;  // Ends the switch case.
            case 3148:
                printf("He reluctantly agrees. He tells you that you were a test subject \nfor an experiment, but he won’t reveal what its purpose was. He tells \nyou that you were stuck in phase two, meaning you kept repeating the \nsame events over and over until you could move on to phase three. \nThen, he presses a button and…\n");
                endingHeader("THE TIME LOOP:");
                printf("You find out you were stuck in a time loop, but were \nreset before you could do anything. You restart with no memory of the \npast events.");
                uuid = 1;
                break;
            case 5758:
                endingHeader("THE HAT:");
                printf("I told you the hat was irrelevant. Can you read?");
                uuid = 1;
                break;
            case 8391:
                endingHeader("DEATH BY MONOLITH:");
                printf("You enter the box, and the solid rock \nliquifies. The liquid takes over the whole space, swallowing you \nwhole. As is common when a rock swallows a human whole, you die.");
                uuid = 1;
                break;
            case 6098:
                printf("You eat the cupcake, and everything seems normal, except for the \nsudden urge to eat more cupcakes. (not due to the cupcake \nspecifically, it’s just that it was a good cupcake.)\n"); 
                endingHeader("DON'T DO DRUGS:");
                printf("Then, you look up, and a clearing in the clouds \nreveals Mr. Corleto in the sky, telling you not to do drugs. You pass \nout, and wake up in a hospital.");
                uuid = 1;
                break;
            case 9109:
                printf("You enter the dull door, and it’s just another maze. You \ncomplete that maze, and then another, and another. \n");
                endingHeader("THE MAZE LOOP:");
                printf("You are stuck completing mazes for the rest of your \nlife, without any breaks, unknowingly training a machine learning \nmodel for self-driving cars. The robots conducting the simulation did \nnot account for the fact that humans need certain things to survive, \nso after a few days of malnourishment, you fall over and take your \nlast breath.");
                uuid = 1;
                break;
            case 3972:
                printf("You enter a room with a single light and a packet of silica gel \non the floor. You are given instructions over a hidden speaker: 'Eat \nit – it’s not like you have anything better to do.' You hear an \naudible click at the door – it just locked. Finally, you hear what you \nthink is a couple people laughing before the message cuts off. As the \nmessage said, you didn’t have anything better to do, so you \nreluctantly ate it.\n");
                endingHeader("BYE BYE, CRUEL SIMULATION:");
                printf("You wake up in your own bed, remove your \nOculus Contacts™, say your daily prayers to the Prime Minister of \nthe Republic of Earth, Mark Zuckerberg VI, and head off to your job \nworking for the USNC to go fight some alien communists.");
                uuid = 1;
                break;
        }
    }
    gameOver(); // Asks the user if they want to play again.
}
