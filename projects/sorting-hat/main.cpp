// Sorting Hat Project v2.0
// @author Sean O'Connor=

#include <iostream>
#include <string.h>

using namespace std;

int g = 0, r = 0, h = 0, s = 0, maxValue;
int house[4]{g,r,h,s};  // Creates an array with 4 values, one for each house.
string result = "undefined";  // This is the string that eventually gets printed with the house name.

void assignPoints(int a) {  // Takes in an integer (a) which is obtained through cin, and increments the correct house.
  switch (a) { 
    case 1:
      house[0]++;
      break;
    case 2:
      house[1]++;
      break;
    case 3:
      house[2]++;
      break;
    case 4:
      house[3]++;
      break;
  }
}

void inputInvalid() { // This is run if the input number isn't 1 through 4. It clears the screen, sends a message, and tries again.
  system("clear"); // Used to clear screen for appearance purposes. WILL ONLY WORK ON UNIX-BASED SYSTEMS
  cout << "Invalid Input." << endl << "Please try again." << endl << " " << endl;
}

void calcResults(){ //This is used for the final calculation of the house points.
  maxValue = house[0];  // Sets the maxValue to the first house.
  for(int i = 0; i < 4; i++){ // Runs 4 times, checks if the maxValue truly is the maximum. If not, it is set equal to the actual max value.
    if(house[i] > maxValue){
      maxValue = house[i];
    }
  }
  if (maxValue == house[0]){  // Now that the maxValue is the largest value, it sets the string to the correct house name. 
    result = "Gryffindor";
  } else if (maxValue == house[1]){
    result = "Ravenclaw";
  } else if (maxValue == house[2]){
    result = "Hufflepuff";
  } else if (maxValue == house[3]){
    result = "Slytherin";
  } else {
    result = "Undefined";
  }
}

void printQuestion(int q){  // Takes in question number, and sets strings/prints accordingly. 
  string question, choice1, choice2, choice3, choice4;
  int a;

  switch (q) { // switch case that sets strings based on question number
    case 1:
      question = "When I'm dead, I want people to remember me as:";
      choice1 = "The Bold"; 
      choice2 = "The Wise";
      choice3 = "The Good";
      choice4 = "The Great";
      break;
    case 2:
      question = "Four goblets are placed before you. Which would you choose to drink?";
      choice1 = "The golden liquid so bright that it hurts the eye, and which makes sunspots dance all around the room."; 
      choice2 = "The foaming, frothing, silvery liquid that sparkles as though containing ground diamonds.";
      choice3 = "The smooth, thick, richly purple drink that gives off a delicious smell of chocolate and plums.";
      choice4 = "The mysterious black liquid that gleams like ink, and gives off fumes that make you see strange visions.";
      break;
    case 3:
      question = "Which road tempts you most?";
      choice1 = "The twisting, leaf-strewn path through woods"; 
      choice2 = "The cobbled street lined with ancient buildings";
      choice3 = "The wide, sunny grassy lane";
      choice4 = "The narrow, dark, lantern-lit alley";
      break;
    case 4:
      question = "Which kind of instrument most pleases your ear?";
      choice1 = "The drum"; 
      choice2 = "The piano";
      choice3 = "The trumpet";
      choice4 = "The violin";
      break;
    case 5:
      question = "You enter an enchanted garden. What would you be most curious to examine first?";
      choice1 = "The statue of an old wizard with a strangely twinkling eye"; 
      choice2 = "The silver leafed tree bearing golden apples";
      choice3 = "The fat red toadstools that appear to be talking to each other";
      choice4 = "The bubbling pool, in the depths of which something luminous is swirling";
      break;
    case 6:
      question = "Four boxes are placed before you. Which would you try and open?";
      choice1 = "1) The small pewter box, unassuming and plain, with a scratched message upon it that reads ‘I open only for the worthy."; 
      choice2 = "The ornate golden casket, standing on clawed feet, whose inscription warns that both secret knowledge and unbearable temptation lie within.";
      choice3 = "The small tortoiseshell box, embellished with gold, inside which some small creature seems to be squeaking.";
      choice4 = "The gleaming jet black box with a silver lock and key, marked with a mysterious rune that you know to be the mark of Merlin.";
      break;
    case 7:
      question = "Which nightmare would frighten you most?";
      choice1 = "An eye at the keyhole of the dark, windowless room in which you are locked."; 
      choice2 = "Standing on top of something very high and realizing suddenly that there are no hand- or footholds, nor any barrier to stop you falling.";
      choice3 = "Waking up to find that neither your friends nor your family have any idea who you are.";
      choice4 = "Being forced to speak in such a silly voice that hardly anyone can understand you, and everyone laughs at you.";
      break;
    case 8:
      question = "A Muggle confronts you and says that they are sure you are a witch or wizard. Do you:";
      choice1 = "Agree, and walk away, leaving them to wonder whether you are bluffing?"; 
      choice2 = "Ask what makes them think so?";
      choice3 = "Tell them that you are worried about their mental health, and offer to call a doctor.";
      choice4 = "Agree, and ask whether they'd like a free sample of a jinx?";
      break;
    case 9:
      question = "You and two friends need to cross a bridge guarded by a river troll who insists on fighting one of you before he will let all of you pass. Do you:";
      choice1 = "Volunteer to fight? "; 
      choice2 = "Attempt to confuse the troll into letting all three of you pass without fighting? ";
      choice3 = "Suggest drawing lots to decide which of you will fight? ";
      choice4 = "Suggest that all three of you should fight (without telling the troll)? ";
      break;
    case 10:
      question = "Late at night, walking alone down the street, you hear a peculiar cry that you believe to have a magical source. Do you:";
      choice1 = "Draw your wand and try to discover the source of the noise? "; 
      choice2 = "Withdraw into the shadows to await developments, while mentally reviewing the most appropriate defensive and offensive spells, should trouble occur?";
      choice3 = "Proceed with caution, keeping one hand on your concealed wand and an eye out for any disturbance? ";
      choice4 = "Draw your wand and stand your ground? ";
      break;
    }
  start:  // for goto staement, if the input is invalid.
  cout << question << endl << " " << endl << "1) " << choice1 << endl << "2) " << choice2 << endl << "3) " << choice3 << endl << "4) " << choice4 << endl << " " << endl << "Type your choice: "; // Prints question data, then propmts for answer.
  cin >> a; // Takes in answer choice
  if (a >= 1 && a <= 4) { // Checks if answer choice is valid
    assignPoints(a);  // If it is, assign the points accordingly
  } else {  // If it isn't, run the inputInvalid method and then try it again.
    inputInvalid(); // Calls the command 'inputInvalid()'
    goto start;
  }
}

int main() {
  for (int i = 1; i < 11; i++){ // Runs 10 times, feeds the question number to the printQuestion method.
    system("clear");  // Clears the screen
    if (i == 1){  // Checks if it is the first question, if it is, print a special header.
      cout << "=====================" << endl << "The Sorting Hat Quiz!" << endl << "=====================" << endl;
    } else {
      cout << "=====================" << endl << "     Question " << i << ":" << endl << "=====================" << endl;
    }
    printQuestion(i); // Runs the printQuestion command with the correct question number.
}

calcResults();  // Finds maximum house value, then sets a 'result' string.
system("clear"); // Clears screen
cout << "=====================" << endl << "      Results:" << endl << "=====================" << endl << "Your house is: " << result << "!" << endl << " " << endl << "Score Breakdown:" << endl << "Gryffindor: " << house[0] << " points." << endl << "Ravenclaw: " << house[1] << " points." << endl << "Hufflepuff: " << house[2] << " points." << endl << "Slytherin: " << house[3] << " points." << endl << " " << endl << "Thanks for playing!" << endl;  // Prints results
}